package com.example.android.fitbuddy_dev;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SwitchCompat;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    TextView edit_profile, logout, setpasstext, our_location;
    EditText setpassword, confirmpassword;
    Button setpass_btn;
    SwitchCompat switch_night_mode, switch_font_size;
    LinearLayout set_password_bottom_sheets;
    BottomSheetDialog bottomSheetDialog;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    TextInputLayout textInputpass1, textInputpass2;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {
        edit_profile = view.findViewById(R.id.edit_profile);
        logout = view.findViewById(R.id.logout);
        switch_font_size = view.findViewById(R.id.switch_font_size);
        our_location = view.findViewById(R.id.our_location);

        //set layout inflater view
        View inflate = LayoutInflater.from(getActivity())
                .inflate(R.layout.set_password_bottom_sheets, null);

        setpassword = inflate.findViewById(R.id.setpassword);
        confirmpassword = inflate.findViewById(R.id.confirmpassword);
        textInputpass1 = inflate.findViewById(R.id.textInputpass1);
        textInputpass2 = inflate.findViewById(R.id.textInputpass2);
        setpasstext = inflate.findViewById(R.id.setpasstext);


        //declaration preferences
        setting = this.getActivity().getSharedPreferences("setting", Context.MODE_PRIVATE);
        editor = setting.edit();

        boolean checked_fontsize = setting.getBoolean("checked_fontsize", false);
        switch_font_size.setChecked(checked_fontsize);

        if(bottomSheetDialog == null){
            bottomSheetDialog = new BottomSheetDialog(getActivity());
            bottomSheetDialog.setContentView(inflate);
        }

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.show();
            }
        });

        if(setting.contains("password")){
            setpasstext.setText("Change Password");
            textInputpass1.setHint("Old Password");
            textInputpass2.setHint("New Password");
        }

        setpass_btn = inflate.findViewById(R.id.setpass_btn);
        setpass_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String setpass = setpassword.getText().toString();
                final String confirmpass = confirmpassword.getText().toString();

                if(!setting.contains("password")){

                    if(setpass.isEmpty() && confirmpass.isEmpty()){
                        Toast.makeText(getActivity(), "Fill all Data", Toast.LENGTH_SHORT).show();
                    }else if(!setpass.equals(confirmpass)){
                        Toast.makeText(getActivity(), "Confirm password must be the sama", Toast.LENGTH_SHORT).show();
                    }else{
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Set Password");
                        alertDialog.setMessage("are you sure ?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putString("password", setpass);
                                editor.apply();
                                setpassword.setText("");
                                confirmpassword.setText("");
                                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                bottomSheetDialog.dismiss();

                            }
                        }).setNegativeButton("No", null).show();

                    }
                }else{
                    String currentpass = setting.getString("password", "");
                    if(!setpass.equals(currentpass)){
                        Toast.makeText(getActivity(), "Wrong Password", Toast.LENGTH_SHORT).show();
                    }else{
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Set Password");
                        alertDialog.setMessage("are you sure you want to change your password ?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putString("password", confirmpass);
                                editor.apply();
                                setpassword.setText("");
                                confirmpassword.setText("");
                                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                bottomSheetDialog.dismiss();

                            }
                        }).setNegativeButton("No", null).show();
                    }


                }

            }
        });

        switch_font_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switch_font_size.isChecked()){
                    Toast.makeText(getActivity(), "font size on", Toast.LENGTH_SHORT).show();
                    editor.putInt("fontsizebig", 16);
                    editor.putBoolean("checked_fontsize", true);
                    editor.apply();
                }else{
                    Toast.makeText(getActivity(), "font size off", Toast.LENGTH_SHORT).show();
                    editor.putInt("fontsizebig", 12);
                    editor.putBoolean("checked_fontsize", false);
                    editor.apply();
                }

            }
        });

        our_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GoogleMapsActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });


    }

    public void logout(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("are you sure you want to logout?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseAuth.getInstance().signOut();
                getActivity().finish();
                editor.clear().commit();
                startActivity(new Intent(getActivity(), HomeActivity.class));

            }
        }).setNegativeButton("No", null).show();

    }

}
