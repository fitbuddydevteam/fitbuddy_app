package com.example.android.fitbuddy_dev;

public class buy_data {
    String transaction_code, uid, packet_id, harga_total, amount, duration, courier, address, status;

    public buy_data(String transaction_code, String uid, String packet_id, String harga_total, String amount, String duration, String courier, String address, String status) {
        this.transaction_code = transaction_code;
        this.uid = uid;
        this.packet_id = packet_id;
        this.harga_total = harga_total;
        this.amount = amount;
        this.duration = duration;
        this.courier = courier;
        this.address = address;
        this.status = status;
    }
}
