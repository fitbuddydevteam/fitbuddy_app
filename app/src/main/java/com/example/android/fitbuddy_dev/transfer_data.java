package com.example.android.fitbuddy_dev;

public class transfer_data {
    String uid, imgUrl, transaction_code, status;

    public transfer_data(String uid, String imgUrl, String transaction_code, String status) {
        this.uid = uid;
        this.imgUrl = imgUrl;
        this.transaction_code = transaction_code;
        this.status = status;
    }
}
