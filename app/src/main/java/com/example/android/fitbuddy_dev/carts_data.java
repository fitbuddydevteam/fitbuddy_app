package com.example.android.fitbuddy_dev;

public class carts_data {
    String id, imgUrl, productname, productprice, amount, total;

    public carts_data(String id, String imgUrl, String productname, String productprice, String amount, String total) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.productname = productname;
        this.productprice = productprice;
        this.amount = amount;
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getProductname() {
        return productname;
    }

    public String getProductprice() {
        return productprice;
    }

    public String getAmount() {
        return amount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
