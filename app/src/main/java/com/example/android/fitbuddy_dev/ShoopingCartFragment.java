package com.example.android.fitbuddy_dev;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoopingCartFragment extends Fragment {

    private RecyclerView rv_cart;
    private carts_adapter adapter;
    ArrayList<carts_data> dataList;
    private ProgressDialog dialog;
    int sum = 0;

    TextView totalPrice;

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
    String uid = currentUser.getUid();

    public ShoopingCartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shooping_cart, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        rv_cart = view.findViewById(R.id.rv_carts);
        totalPrice = view.findViewById(R.id.totalPrice);
        dialog = new ProgressDialog(getActivity());

        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query carts = rootRef.child("carts").orderByChild("uid").equalTo(uid);

        dataList = new ArrayList<>();
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int sum = 0;
                dialog.show();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if(dataSnapshot.hasChildren()){
                        final String id = ds.getKey();
                        final String amount = ds.child("amount").getValue(String.class);
                        final String hargatotal = ds.child("harga").getValue(String.class);
                        final String uid = ds.child("uid").getValue(String.class);
                        final String fitfood_id = ds.child("fitfood_id").getValue(String.class);

                        Map<String, Object> map = (Map<String, Object>) ds.getValue();
                        Object totals = map.get("harga");
                        int value = Integer.parseInt(String.valueOf(totals));
                        sum += value;

                        totalPrice.setText(String.valueOf(sum));

                        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                        Query fitfood = rootRef.child("fit-food").orderByChild("id").equalTo(fitfood_id);
                        ValueEventListener valueEventListener = new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.hasChildren()) {
                                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                        String deskripsi = ds.child("deskripsi").getValue(String.class);
                                        String harga = ds.child("harga").getValue(String.class);
                                        String imgUrl = ds.child("imgUrl").getValue(String.class);
                                        String name = ds.child("name").getValue(String.class);
                                        String stok = ds.child("stok").getValue(String.class);

                                        dataList.add(new carts_data(id, imgUrl, name, harga, amount, hargatotal));
                                        adapter = new carts_adapter(dataList, getActivity());

                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                        rv_cart.setLayoutManager(layoutManager);
                                        rv_cart.setAdapter(adapter);
                                        dialog.dismiss();

                                    }
                                }else{
                                    dialog.dismiss();
                                }


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        };
                        fitfood.addValueEventListener(valueEventListener);

                    }else{
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "no data",Toast.LENGTH_SHORT).show();
                    }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };

        carts.addValueEventListener(eventListener);
    }


}
