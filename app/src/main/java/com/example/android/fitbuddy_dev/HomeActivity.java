package com.example.android.fitbuddy_dev;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView bottom_navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        checkUserLogin();

        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment seledtedFragment = null;

            switch (menuItem.getItemId()){
                case R.id.home:
                    seledtedFragment = new HomeFragment();
                    break;
                case R.id.shoping_cart:
                    seledtedFragment = new ShoopingCartFragment();
                    break;
                case R.id.profile:
                    seledtedFragment = new ProfileFragment();
                    break;
                case R.id.setting:
                    seledtedFragment = new SettingFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, seledtedFragment).commit();
            return true;
        }
    };

    public void checkUserLogin(){
        if(FirebaseAuth.getInstance().getCurrentUser() == null){
            Intent intent = new Intent(this, Activity_login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            HomeActivity.this.startActivity(intent);
            finish();
        }
    }

    public void setToastMsg(String message){
        Toast toast = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT);
        toast.show();
    }
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().getCurrentUser();
    }


    @Override
    protected void onResume() {
        super.onResume();
        FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().getCurrentUser();
    }
}
