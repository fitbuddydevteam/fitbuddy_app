package com.example.android.fitbuddy_dev;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailFoodActivity extends AppCompatActivity {

    TextView name, price, stok, deskripsi, showMoreButton;
    ImageView img;
    Button shopping_cart_button, buy;

    String names, prices, imgUrls,stoks,deskripsis;

    SharedPreferences setting;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_food);

        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        stok = findViewById(R.id.stok);
        img = findViewById(R.id.img);
        deskripsi = findViewById(R.id.deskripsi);
        showMoreButton = findViewById(R.id.showMoreButton);
        shopping_cart_button = findViewById(R.id.shopping_cart_button);
        buy = findViewById(R.id.buy);

        //declaration preferences
        setting = this.getSharedPreferences("setting", Context.MODE_PRIVATE);

        int fontsize = setting.getInt("fontsizebig", 12);
        deskripsi.setTextSize(fontsize);


        if(getIntent().hasExtra("id")){
            String id = getIntent().getStringExtra("id");
            DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
            final DatabaseReference fithealth = rootRef.child("fit-food").child(id);
            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {

                    names = ds.child("name").getValue(String.class);
                    prices = ds.child("harga").getValue(String.class);
                    imgUrls = ds.child("imgUrl").getValue(String.class);
                    stoks = ds.child("stok").getValue(String.class);
                    deskripsis = ds.child("deskripsi").getValue(String.class);

                    name.setText(names);
                    price.setText("Rp." + prices);
                    Glide.with(DetailFoodActivity.this)
                            .load(imgUrls)
                            .into(img);
                    stok.setText(stoks);
                    deskripsi.setText(deskripsis);



                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            };
            fithealth.addListenerForSingleValueEvent(eventListener);
        }

        showMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(names, deskripsis);
            }
        });

        shopping_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addCart = new Intent(getApplicationContext(), AddCartActivity.class);
                addCart.putExtra("id", getIntent().getStringExtra("id"));
                DetailFoodActivity.this.startActivity(addCart);
            }
        });

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailFoodActivity.this, BuyActivity.class);
                intent.putExtra("id", getIntent().getStringExtra("id"));
                DetailFoodActivity.this.startActivity(intent);
            }
        });


    }

    public void alert(String name, String deskripsi){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(name + "'s Description");
        // set dialog message
        alertDialogBuilder
                .setMessage(deskripsi)
                .setCancelable(false)
                .setPositiveButton("Close", null);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();



    }

}
