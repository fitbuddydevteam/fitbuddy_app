package com.example.android.fitbuddy_dev;

public class register {
    private String uid,
            firstname, lastname, email, phone_number,
            username, password;

    public register(String uid, String firstname, String lastname, String email, String phone_number, String username, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone_number = phone_number;
        this.username = username;
        this.password = password;
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
