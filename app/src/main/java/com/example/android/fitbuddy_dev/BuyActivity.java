package com.example.android.fitbuddy_dev;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class BuyActivity extends AppCompatActivity{

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
    String uid = currentUser.getUid();

    private static final String[] duration = {"Reguler (2 - 4 Hari)"};
    private static final String[] courierReguler = {"J&T Rp. 11.000", "JNE Reg Rp. 11.000"};

    TextView user_full_name, address_shipping;
    Button button_edit_destination, btncheckout;
    Spinner duration_spinner, courier_spinner;
    ImageView img, plus, minus;
    TextView productname, productprice, totalPrice;
    EditText jumlah;

    String getDuration;
    String getCourier;
    String names, prices, imgUrls, stoks;
    int getPrice;

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);

        user_full_name = findViewById(R.id.user_full_name);
        address_shipping = findViewById(R.id.address_shipping);
        button_edit_destination = findViewById(R.id.button_edit_destination);
        duration_spinner = findViewById(R.id.duration_spinner);
        courier_spinner = findViewById(R.id.courier_spinner);
        jumlah = findViewById(R.id.jumlah);
        img = findViewById(R.id.img);
        plus = findViewById(R.id.plus);
        minus = findViewById(R.id.minus);
        productname = findViewById(R.id.productname);
        productprice = findViewById(R.id.productprice);
        jumlah = findViewById(R.id.jumlah);
        totalPrice = findViewById(R.id.totalPrice);
        btncheckout = findViewById(R.id.btncheckout);

        getUserInfo();
        button_edit_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAddress();
            }
        });

        //set default value jumlah
        jumlah.setText("1");

        getdata();

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseJumlah();
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreaseJumlah();
            }
        });

        durationSpinner();
        courierSpinner();

        btncheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout();
            }
        });


    }
    public void checkout(){
        if(address_shipping.getText().equals("Address not set")){
            Toast.makeText(this, "Please add address first", Toast.LENGTH_SHORT).show();
        }else{
            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference("payments");

            //generate random transaction code
            String uid = currentUser.getUid();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String currentdate = sdf.format(new Date());
            String transaction_code = uid+currentdate;

            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;


            String fitfood_id = getIntent().getStringExtra("id");
            String harga = totalPrice.getText().toString();
            String amount = jumlah.getText().toString();
            String address = address_shipping.getText().toString();
            String status = "unpaid";

            buy_data buy_data = new buy_data(transaction_code, uid,fitfood_id,harga,amount, getDuration, getCourier, address, status);

            databaseReference.child(transaction_code).setValue(buy_data).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(BuyActivity.this, ProfileActivity.class);
                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    public void getdata(){
        if(getIntent().hasExtra("id")){
            String id = getIntent().getStringExtra("id");
            DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
            final DatabaseReference fitfood = rootRef.child("fit-food").child(id);
            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {

                    names = ds.child("name").getValue(String.class);
                    prices = ds.child("harga").getValue(String.class);
                    imgUrls = ds.child("imgUrl").getValue(String.class);
                    stoks = ds.child("stok").getValue(String.class);

                    productname.setText(names);
                    productprice.setText("Rp." + prices);
                    Glide.with(BuyActivity.this)
                            .load(imgUrls)
                            .into(img);
                    totalPrice.setText(prices);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            };
            fitfood.addListenerForSingleValueEvent(eventListener);
        }
    }

    public void increaseJumlah(){
        if(Integer.parseInt(jumlah.getText().toString()) == 1 ||
                Integer.parseInt(jumlah.getText().toString()) != Integer.parseInt(stoks)){

            int getJumlah = Integer.parseInt(jumlah.getText().toString()) + 1;
            jumlah.setText(String.valueOf(getJumlah));
            int getTotal = Integer.parseInt(totalPrice.getText().toString()) + Integer.parseInt(prices);
            totalPrice.setText(String.valueOf(getTotal));


        }else{
            Toast.makeText(this, "max " + stoks, Toast.LENGTH_SHORT).show();
        }


    }

    public void decreaseJumlah(){
        if(Integer.parseInt(jumlah.getText().toString()) > 1){

            int getJumlah = Integer.parseInt(jumlah.getText().toString()) - 1;
            jumlah.setText(String.valueOf(getJumlah));
            int getTotal = Integer.parseInt(totalPrice.getText().toString())  - Integer.parseInt(prices);
            totalPrice.setText(String.valueOf(getTotal));

        }else{
            Toast.makeText(this, "min 1", Toast.LENGTH_SHORT).show();
        }

    }

    public void durationSpinner(){
        //duration_spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BuyActivity.this,
                android.R.layout.simple_spinner_item, duration);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        duration_spinner.setAdapter(adapter);
        duration_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    getDuration = "Reguler (2 - 4 Hari)";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void courierSpinner(){
        ArrayAdapter<String> adaptercourier = new ArrayAdapter<String>(BuyActivity.this,
                android.R.layout.simple_spinner_item, courierReguler);
        adaptercourier.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        courier_spinner.setAdapter(adaptercourier);
        courier_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    getCourier = "J&T Rp. 11.000";
                    int getTotal = Integer.parseInt(totalPrice.getText().toString()) + 11000;
                    totalPrice.setText(String.valueOf(getTotal));

                }else{
                    getCourier = "JNE Rp. 11.000";
                    int getTotal = Integer.parseInt(totalPrice.getText().toString()) + 11000;
                    totalPrice.setText(String.valueOf(getTotal));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getUserInfo(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query exercise = rootRef.child("user").orderByChild("uid").equalTo(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String getfullname = ds.child("firstname").getValue(String.class) + " " + ds.child("lastname").getValue(String.class);
                    user_full_name.setText(getfullname);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }

    public void addAddress(){
        dialog = new AlertDialog.Builder(BuyActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.item_custom_dialog, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Form Add Address");

        final EditText address = (EditText) dialogView.findViewById(R.id.address);
        Button addbutton = (Button) dialogView.findViewById(R.id.addbutton);

        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getAddress = address.getText().toString();

                if(!getAddress.isEmpty()){
                    address_shipping.setText(getAddress);
                    Toast.makeText(BuyActivity.this, "Success", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.setNegativeButton("CANCEL",null);

        dialog.show();
    }
}
