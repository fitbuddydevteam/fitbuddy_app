package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class fit_food extends AppCompatActivity {

    private RecyclerView rv_list_food;
    private fit_food_adapter adapter;
    private ArrayList<fit_food_data> foodArrayList;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fit_food);

        new FoodData(this).execute();


    }

    private class FoodData extends AsyncTask<Void, Void, Void> {


        public FoodData(fit_food activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Doing something, please wait.");
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... args) {
            readFoodData();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // do UI work here


        }
    }

    public void readFoodData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference fitfood = rootRef.child("fit-food");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                foodArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("name").getValue(String.class);
                    String price = ds.child("harga").getValue(String.class).toString();
                    String imgUrl = ds.child("imgUrl").getValue(String.class);


                    foodArrayList.add(new fit_food_data(imgUrl, title, price, id));


                    adapter = new fit_food_adapter(foodArrayList, fit_food.this);
                    rv_list_food = findViewById(R.id.rv_list_food);

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(fit_food.this, 2);
                    rv_list_food.setLayoutManager(layoutManager);
                    rv_list_food.setAdapter(adapter);
                    dialog.dismiss();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        fitfood.addListenerForSingleValueEvent(eventListener);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.shoppingCart:
                    startActivity(new Intent(this, CartsActivity.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    public void setToastMsg(String message){
        Toast toast = Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT);
        toast.show();
    }
}
