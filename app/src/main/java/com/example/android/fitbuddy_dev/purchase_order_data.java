package com.example.android.fitbuddy_dev;

public class purchase_order_data {
    String imgUrl, amount, harga_total, packet_id, transaction_code, uid, product_name, status;

    public purchase_order_data(String imgUrl, String amount, String harga_total, String packet_id, String transaction_code, String uid, String product_name, String status) {
        this.imgUrl = imgUrl;
        this.amount = amount;
        this.harga_total = harga_total;
        this.packet_id = packet_id;
        this.transaction_code = transaction_code;
        this.uid = uid;
        this.product_name = product_name;
        this.status = status;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getAmount() {
        return amount;
    }

    public String getHarga_total() {
        return harga_total;
    }

    public String getPacket_id() {
        return packet_id;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public String getUid() {
        return uid;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getStatus() {
        return status;
    }
}
