package com.example.android.fitbuddy_dev;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class exercise_adapter extends RecyclerView.Adapter<exercise_adapter.exerciseViewHolder>  {

    private ArrayList<exercise_data> dataList;
    Context context;
    public exercise_adapter(Context context, ArrayList<exercise_data> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public exerciseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_video, viewGroup, false);
        return new exerciseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull exerciseViewHolder foodViewHolder, int i) {
        foodViewHolder.title.setText(dataList.get(i).getTitle());
        foodViewHolder.date.setText(dataList.get(i).getDate());
        foodViewHolder.id = dataList.get(i).getId();
        Glide.with(context)
                .load(dataList.get(i).getImguRL())
                .into(foodViewHolder.img);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public class exerciseViewHolder extends RecyclerView.ViewHolder{

        private ImageView img;
        private TextView title;
        private TextView date;
        private String id;

        public exerciseViewHolder(@NonNull final View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            title = itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.date);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), DetailVideo.class);
                    intent.putExtra("id", id);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
