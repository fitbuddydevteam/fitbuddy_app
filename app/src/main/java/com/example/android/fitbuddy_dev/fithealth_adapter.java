package com.example.android.fitbuddy_dev;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class fithealth_adapter extends RecyclerView.Adapter<fithealth_adapter.foodViewHolder>  {

    private ArrayList<fithealth_data> dataList;
    Context context;
    public fithealth_adapter(ArrayList<fithealth_data> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public foodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_fithealth, viewGroup, false);
        return new foodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull foodViewHolder foodViewHolder, int i) {
        foodViewHolder.title.setText(dataList.get(i).getTitle());
        foodViewHolder.price.setText(dataList.get(i).getPrice());
        foodViewHolder.id = dataList.get(i).getId();
        Glide.with(context)
                .load(dataList.get(i).getImg())
                .into(foodViewHolder.img);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public class foodViewHolder extends RecyclerView.ViewHolder{

        private ImageView img;
        private TextView title;
        private TextView price;
        private String id;

        public foodViewHolder(@NonNull final View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), detail_article_activity.class);
                    intent.putExtra("id", id);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
