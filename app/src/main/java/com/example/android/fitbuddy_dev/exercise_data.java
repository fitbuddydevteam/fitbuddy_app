package com.example.android.fitbuddy_dev;

public class exercise_data {

    private String title, date, imguRL, id;

    public exercise_data(String title, String date, String imguRL, String id) {
        this.title = title;
        this.date = date;
        this.imguRL = imguRL;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImguRL() {
        return imguRL;
    }

    public void setImguRL(String imguRL) {
        this.imguRL = imguRL;
    }
}
