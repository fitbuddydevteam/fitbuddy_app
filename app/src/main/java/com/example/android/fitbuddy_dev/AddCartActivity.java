package com.example.android.fitbuddy_dev;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddCartActivity extends AppCompatActivity {

    ImageView img, plus, minus;
    TextView productname, productprice, totalPrice;
    EditText jumlah;
    Button add_to_cart;

    String names, prices, imgUrls, stoks;

    //deklarasi Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);

        img = findViewById(R.id.img);
        plus = findViewById(R.id.plus);
        minus = findViewById(R.id.minus);
        productname = findViewById(R.id.productname);
        productprice = findViewById(R.id.productprice);
        jumlah = findViewById(R.id.jumlah);
        totalPrice = findViewById(R.id.totalPrice);
        add_to_cart = findViewById(R.id.add_to_cart);

        //set default value jumlah
        jumlah.setText("1");

        getCartIntent();

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseJumlah();
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreaseJumlah();
            }
        });

        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_to_cart();
            }
        });



    }
    public void getCartIntent(){
        if(getIntent().hasExtra("id")){
            String id = getIntent().getStringExtra("id");
            DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
            final DatabaseReference fitfood = rootRef.child("fit-food").child(id);
            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {

                    names = ds.child("name").getValue(String.class);
                    prices = ds.child("harga").getValue(String.class);
                    imgUrls = ds.child("imgUrl").getValue(String.class);
                    stoks = ds.child("stok").getValue(String.class);

                    productname.setText(names);
                    productprice.setText("Rp." + prices);
                    Glide.with(AddCartActivity.this)
                            .load(imgUrls)
                            .into(img);
                    totalPrice.setText(prices);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            };
            fitfood.addListenerForSingleValueEvent(eventListener);
        }
    }

    public void increaseJumlah(){
        if(Integer.parseInt(jumlah.getText().toString()) == 1 ||
                Integer.parseInt(jumlah.getText().toString()) != Integer.parseInt(stoks)){

            int getJumlah = Integer.parseInt(jumlah.getText().toString()) + 1;
            jumlah.setText(String.valueOf(getJumlah));
            int getTotal = Integer.parseInt(totalPrice.getText().toString()) + Integer.parseInt(prices);
            totalPrice.setText(String.valueOf(getTotal));


        }else{
            Toast.makeText(this, "max " + stoks, Toast.LENGTH_SHORT).show();
        }


    }

    public void decreaseJumlah(){
        if(Integer.parseInt(jumlah.getText().toString()) > 1){

            int getJumlah = Integer.parseInt(jumlah.getText().toString()) - 1;
            jumlah.setText(String.valueOf(getJumlah));
            int getTotal = Integer.parseInt(totalPrice.getText().toString())  - Integer.parseInt(prices);
            totalPrice.setText(String.valueOf(getTotal));

        }else{
            Toast.makeText(this, "min 1", Toast.LENGTH_SHORT).show();
        }

    }

    public void add_to_cart(){
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("carts");



        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String uid = currentUser.getUid();

        String fitfood_id = getIntent().getStringExtra("id");
        String harga = totalPrice.getText().toString();
        String amount = jumlah.getText().toString();

        add_to_cart add_to_cart = new add_to_cart(uid,fitfood_id,harga,amount);

        databaseReference.push().setValue(add_to_cart).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "success add item to shopping cart", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AddCartActivity.this, fit_food.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
}
