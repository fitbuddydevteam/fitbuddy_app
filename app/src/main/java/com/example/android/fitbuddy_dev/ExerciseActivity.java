package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ExerciseActivity extends AppCompatActivity {

    private RecyclerView rv_video;
    private exercise_adapter adapter;
    private ArrayList<exercise_data> exerciseArrayList;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        new ExerciseData(this).execute();

    }

    private class ExerciseData extends AsyncTask<Void, Void, Void> {


        public ExerciseData(ExerciseActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Doing something, please wait.");
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... args) {
            readExerciseData();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // do UI work here


        }
    }

    public void readExerciseData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference exercise = rootRef.child("exercise");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                exerciseArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("title").getValue(String.class);
                    String date = ds.child("date").getValue(String.class);
                    String imgUrl = ds.child("thumbnailUrl").getValue(String.class);


                    exerciseArrayList.add(new exercise_data(title, date, imgUrl, id));


                    adapter = new exercise_adapter (ExerciseActivity.this, exerciseArrayList);
                    rv_video = findViewById(R.id.rv_video);

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(ExerciseActivity.this, 2);
                    rv_video.setLayoutManager(layoutManager);
                    rv_video.setAdapter(adapter);
                    dialog.dismiss();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
