package com.example.android.fitbuddy_dev;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class carts_adapter extends RecyclerView.Adapter<carts_adapter.cartsViewHolder> {

    private ArrayList<carts_data> cartsdataList;
    Context context;
    DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("carts");


    public carts_adapter(ArrayList<carts_data> cartsdataList, Context context) {
        this.cartsdataList = cartsdataList;
        this.context = context;
    }

    @NonNull
    @Override
    public carts_adapter.cartsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_carts, viewGroup, false);
        return new carts_adapter.cartsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull carts_adapter.cartsViewHolder cartsViewHolder, int i) {
        cartsViewHolder.productname_cart.setText(cartsdataList.get(i).getProductname());
        cartsViewHolder.productprice_cart.setText(cartsdataList.get(i).getProductprice());
        cartsViewHolder.jumlah_cart.setText(cartsdataList.get(i).getAmount());
        Glide.with(context)
                .load(cartsdataList.get(i).getImgUrl())
                .into(cartsViewHolder.imgproduct_cart);

        cartsViewHolder.id = cartsdataList.get(i).getId();
    }

    @Override
    public int getItemCount() {
        return cartsdataList.size();
    }
    public class cartsViewHolder extends RecyclerView.ViewHolder{

        ImageView imgproduct_cart, delete_cart;
        TextView productname_cart, productprice_cart, jumlah_cart;
        String id;

        public cartsViewHolder(@NonNull final View itemView) {
            super(itemView);
            imgproduct_cart = itemView.findViewById(R.id.imgproduct_cart);
            delete_cart = itemView.findViewById(R.id.delete_cart);
            productname_cart = itemView.findViewById(R.id.productname_cart);
            productprice_cart = itemView.findViewById(R.id.productprice_cart);
            jumlah_cart = itemView.findViewById(R.id.jumlah_cart);

            delete_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle("Delete Item")
                            .setMessage("Are you sure you want to delete this item?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    DatabaseReference delete = rootRef.child(id);
                                    delete.removeValue();
//                                    Intent intent = new Intent(itemView.getContext(), CartsActivity.class);
//                                    ((Activity)context).finish();
//                                    itemView.getContext().startActivity(intent);


                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .show();

                }
            });


        }
    }
}
