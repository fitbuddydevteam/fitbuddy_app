package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    Button logout;

    private RecyclerView rv_po;
    private purchase_order_data_adapter adapter;
    ArrayList<purchase_order_data> dataList;


    //deklarasi Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
    String uid = currentUser.getUid();
    TextView username, email;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        rv_po = findViewById(R.id.rv_po);

        new ProfileData(this).execute();

    }
    public class ProfileData extends AsyncTask<Void, Void, Void> {


        public ProfileData(ProfileActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Doing something, please wait.");
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... args) {
            readUserData();
            dialog.dismiss();
            getPurchaseOrderData();

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {



        }
    }

    public void readUserData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query exercise = rootRef.child("user").orderByChild("uid").equalTo(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String getfullname = ds.child("firstname").getValue(String.class) + " " + ds.child("lastname").getValue(String.class);
                    String getusername = ds.child("username").getValue(String.class);

                    username.setText(getfullname);
                    email.setText(getusername);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }

    public void getPurchaseOrderData(){

        dataList = new ArrayList<>();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query po = rootRef.child("payments").orderByChild("uid").equalTo(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String address = ds.child("address").getValue(String.class);
                    final String amount = ds.child("amount").getValue(String.class);
                    String courier = ds.child("courier").getValue(String.class);
                    String duration = ds.child("duration").getValue(String.class);
                    final String harga_total = ds.child("harga_total").getValue(String.class);
                    final String packet_id = ds.child("packet_id").getValue(String.class);
                    final String status = ds.child("status").getValue(String.class);
                    final String transaction_code = ds.child("transaction_code").getValue(String.class);
                    final String uid = ds.child("uid").getValue(String.class);

                    DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                    final Query fitfood = rootRef.child("fit-food").orderByChild("id").equalTo(packet_id);
                    ValueEventListener eventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for(DataSnapshot ds : dataSnapshot.getChildren()) {

                                String deskripsi = ds.child("deskripsi").getValue(String.class);
                                String harga = ds.child("harga").getValue(String.class);
                                String id = ds.child("id").getValue(String.class);
                                String imgUrl = ds.child("imgUrl").getValue(String.class);
                                String name = ds.child("name").getValue(String.class);
                                String stok = ds.child("stok").getValue(String.class);

                                dataList.add(new purchase_order_data(imgUrl, amount, harga_total, packet_id, transaction_code, uid, name, status));
                                adapter = new purchase_order_data_adapter(dataList, ProfileActivity.this);

                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ProfileActivity.this);
                                rv_po.setLayoutManager(layoutManager);
                                rv_po.setAdapter(adapter);


                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    };
                    fitfood.addListenerForSingleValueEvent(eventListener);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        po.addListenerForSingleValueEvent(eventListener);

    }

}
