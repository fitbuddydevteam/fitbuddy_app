package com.example.android.fitbuddy_dev;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class detail_article_activity extends AppCompatActivity {

    private RecyclerView rv_list_food, rv_video_related;
    private fit_food_adapter adapter;
    private ArrayList<fit_food_data> foodArrayList;

    private exercise_adapter adapter2;
    private ArrayList<exercise_data> exerciseArrayList;

    TextView title, date, article;
    ImageView img;

    SharedPreferences setting;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article_activity);

        title = findViewById(R.id.title);
        date = findViewById(R.id.date);
        article = findViewById(R.id.article);
        img = findViewById(R.id.img);
        rv_video_related = findViewById(R.id.rv_video_related);

        setting = this.getSharedPreferences("setting", Context.MODE_PRIVATE);
        int fontSize = setting.getInt("fontsizebig", 12);

        article.setTextSize(fontSize);
        article();
        readHealthData();
        related_food();

    }

    public void article(){
        if(getIntent().hasExtra("id")){
            String id = getIntent().getStringExtra("id");
            DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
            final DatabaseReference fithealth = rootRef.child("fit-health").child(id);
            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {

                    String titles = ds.child("title").getValue(String.class);
                    String dates = ds.child("date").getValue(String.class);
                    String imgUrls = ds.child("imgUrl").getValue(String.class);
                    String articles = ds.child("artikel").getValue(String.class);

                    title.setText(titles);
                    date.setText(dates);
                    Glide.with(detail_article_activity.this)
                            .load(imgUrls)
                            .into(img);
                    article.setText(articles);



                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            };
            fithealth.addListenerForSingleValueEvent(eventListener);
        }
    }

    public void related_food(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference fitfood = rootRef.child("fit-food");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                foodArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("name").getValue(String.class);
                    String price = ds.child("harga").getValue(String.class).toString();
                    String imgUrl = ds.child("imgUrl").getValue(String.class);


                    foodArrayList.add(new fit_food_data(imgUrl, title, price, id));


                    adapter = new fit_food_adapter(foodArrayList, detail_article_activity.this);
                    rv_list_food = findViewById(R.id.rv_list_food);

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(detail_article_activity.this, 2);
                    rv_list_food.setLayoutManager(layoutManager);
                    rv_list_food.setAdapter(adapter);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        fitfood.addListenerForSingleValueEvent(eventListener);
    }

    public void readHealthData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query exercise = rootRef.child("exercise").limitToLast(5);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                exerciseArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("title").getValue(String.class);
                    String date = ds.child("date").getValue(String.class);
                    String imgUrl = ds.child("thumbnailUrl").getValue(String.class);


                    exerciseArrayList.add(new exercise_data(title, date, imgUrl, id));
                    adapter2 = new exercise_adapter (detail_article_activity.this, exerciseArrayList);


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(detail_article_activity.this, LinearLayout.HORIZONTAL, false);
                    rv_video_related.setLayoutManager(layoutManager);
                    rv_video_related.setAdapter(adapter2);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }
}
