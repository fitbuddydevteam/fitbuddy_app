package com.example.android.fitbuddy_dev;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Activity_register extends AppCompatActivity {

    //deklarasi komponen layout
    EditText firstname, lastname, email, phone_number, username, password;
    Button signup;
    ProgressBar progressBar;

    //deklarasi Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //deklarasi komponen layout
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        phone_number = findViewById(R.id.phone_number);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        signup = findViewById(R.id.signup);
        progressBar = findViewById(R.id.progressBar);

        //dekalari firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");

        //click listener button
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do_regist();
            }
        });




    }
    public void do_regist(){
        final String getFirstname = firstname.getText().toString();
        final String getLastname = lastname.getText().toString();
        final String getEmail = email.getText().toString();
        final String getPhoneNumber = phone_number.getText().toString();
        final String getUsername = username.getText().toString();
        final String getPassword = password.getText().toString();

        firebaseAuth = FirebaseAuth.getInstance();

        if(!getFirstname.equals("") && !getLastname.equals("")
                && !getEmail.equals("") && !getPhoneNumber.equals("")
                && !getUsername.equals("") && !getPassword.equals("")){

            progressBar.setVisibility(View.VISIBLE);

            firebaseAuth.createUserWithEmailAndPassword(getEmail, getPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){

                        showToast("Login Berhasil");

                        String uid = task.getResult().getUser().getUid();
                        register register = new register(uid, getFirstname,getLastname,getEmail,getPhoneNumber,getUsername, getPassword);
                        databaseReference.child(uid)
                                .setValue(register);
                        Intent intent = new Intent(Activity_register.this, Activity_login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Activity_register.this.startActivity(intent);
                    }else{
                        if(task.getException() instanceof FirebaseAuthUserCollisionException){
                            showToast("email sudah terpakai");
                        }

                    }
                }
            });



        }else{
            showToast("Lengkapi Data");
        }
    }
    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
