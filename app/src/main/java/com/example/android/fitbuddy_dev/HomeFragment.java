package com.example.android.fitbuddy_dev;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    Button fitfood, exercise, fithealth;
    ImageView profile;

    CardView cardView_exercise;

    private RecyclerView rv_fitfood_5;
    private fit_food_adapter adapter;
    private ArrayList<fit_food_data> foodArrayList;
    private ProgressDialog dialog;

    private RecyclerView rv_exercise_5;
    private exercise_adapter adapter2;
    private ArrayList<exercise_data> exerciseArrayList;

    private RecyclerView rv_fithealth_5;
    private fithealth_adapter adapter3;
    private ArrayList<fithealth_data> articleArrayList;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        fitfood = view.findViewById(R.id.fitfood);
        exercise = view.findViewById(R.id.exercise);
        fithealth = view.findViewById(R.id.fithealth);
        rv_fitfood_5 = view.findViewById(R.id.rv_fitfood_5);
        rv_exercise_5 = view.findViewById(R.id.rv_exercise_5);
        rv_fithealth_5 = view.findViewById(R.id.rv_fithealth_5);

        //set layout inflater view
        View inflate = LayoutInflater.from(getActivity())
                .inflate(R.layout.item_video, null);
        cardView_exercise = inflate.findViewById(R.id.cardView_exercise);

        fitfood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), fit_food.class);
                startActivity(intent);
            }
        });

        exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getActivity(), ExerciseActivity.class);
                startActivity(intent);
            }
        });

        fithealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getActivity(), fithealth.class);
                startActivity(intent);
            }
        });

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please Wait...");
        dialog.show();

        readFoodData();
        readExerciseData();
        readHealthData();

    }

    public void readFoodData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query fitfood = rootRef.child("fit-food").limitToLast(5);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                foodArrayList = new ArrayList<>();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("name").getValue(String.class);
                    String price = ds.child("harga").getValue(String.class);
                    String imgUrl = ds.child("imgUrl").getValue(String.class);


                    foodArrayList.add(new fit_food_data(imgUrl, title, price, id));


                    adapter = new fit_food_adapter(foodArrayList, getActivity());


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayout.HORIZONTAL, false);
                    rv_fitfood_5.setLayoutManager(layoutManager);
                    rv_fitfood_5.setAdapter(adapter);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        fitfood.addListenerForSingleValueEvent(eventListener);
    }

    public void readExerciseData(){

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query exercise = rootRef.child("exercise").limitToLast(5);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                exerciseArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("title").getValue(String.class);
                    String date = ds.child("date").getValue(String.class);
                    String imgUrl = ds.child("thumbnailUrl").getValue(String.class);


                    exerciseArrayList.add(new exercise_data(title, date, imgUrl, id));
                    adapter2 = new exercise_adapter (getActivity(), exerciseArrayList);


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayout.HORIZONTAL, false);
                    rv_exercise_5.setLayoutManager(layoutManager);
                    rv_exercise_5.setAdapter(adapter2);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }

    public void readHealthData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query fithealth = rootRef.child("fit-health").limitToLast(5);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                articleArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("title").getValue(String.class);
                    String date = ds.child("date").getValue(String.class);
                    String imgUrl = ds.child("imgUrl").getValue(String.class);


                    articleArrayList.add(new fithealth_data(title, date, imgUrl, id));


                    adapter3 = new fithealth_adapter(articleArrayList, getActivity());


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    rv_fithealth_5.setLayoutManager(layoutManager);
                    rv_fithealth_5.setAdapter(adapter3);
                    dialog.dismiss();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        fithealth.addListenerForSingleValueEvent(eventListener);
    }

}
