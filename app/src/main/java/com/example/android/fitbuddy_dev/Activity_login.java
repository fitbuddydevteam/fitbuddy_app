package com.example.android.fitbuddy_dev;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Activity_login extends AppCompatActivity {
    //deklarasi kompenen layoout
    EditText username, password;
    Button Btn_login;
    TextView signin, btn_forgotpw;
    ProgressBar progressBar;

    //deklarasi Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //deklarasi komponen layout
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        Btn_login = findViewById(R.id.Btn_login);
        signin = findViewById(R.id.signin);
        progressBar = findViewById(R.id.progressBar);
        btn_forgotpw = findViewById(R.id.btn_forgotpw);


        //dekalari firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");


        //click listener button
        Btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do_login();
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do_signup();
            }
        });
        btn_forgotpw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotpw();
            }
        });

    }
    public void do_login(){
        String getUsername = username.getText().toString();
        final String getPassword = password.getText().toString();

        firebaseAuth = FirebaseAuth.getInstance();

        if(!getUsername.equals("") && !getPassword.equals("")) {
            progressBar.setVisibility(View.VISIBLE);

            firebaseAuth.signInWithEmailAndPassword(getUsername, getPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        showToast("Login Berhasil");
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        Activity_login.this.startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }
    public void do_signup(){
        Intent intent = new Intent(this, Activity_register.class);
        this.startActivity(intent);
        finish();
    }
    public void forgotpw(){
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        this.startActivity(intent);
    }
    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
