package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class CartsActivity extends AppCompatActivity {

    private RecyclerView rv_cart;
    private carts_adapter adapter;
    ArrayList<carts_data> dataList;
    private ProgressDialog dialog;
    int sum = 0;

    TextView totalPrice;

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
    String uid = currentUser.getUid();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carts);

        rv_cart = findViewById(R.id.rv_carts);
        totalPrice = findViewById(R.id.totalPrice);

        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query carts = rootRef.child("carts").orderByChild("uid").equalTo(uid);

        dataList = new ArrayList<>();
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int sum = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    final String id = ds.getKey();
                    final String amount = ds.child("amount").getValue(String.class);
                    final String hargatotal = ds.child("harga").getValue(String.class);
                    final String uid = ds.child("uid").getValue(String.class);
                    final String fitfood_id = ds.child("fitfood_id").getValue(String.class);

                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    Object totals = map.get("harga");
                    int value = Integer.parseInt(String.valueOf(totals));
                    sum += value;

                    totalPrice.setText(String.valueOf(sum));

                    DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                    Query fitfood = rootRef.child("fit-food").orderByChild("id").equalTo(fitfood_id);
                    ValueEventListener valueEventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                String deskripsi = ds.child("deskripsi").getValue(String.class);
                                String harga = ds.child("harga").getValue(String.class);
                                String imgUrl = ds.child("imgUrl").getValue(String.class);
                                String name = ds.child("name").getValue(String.class);
                                String stok = ds.child("stok").getValue(String.class);

                                dataList.add(new carts_data(id, imgUrl, name, harga, amount, hargatotal));
                                adapter = new carts_adapter(dataList, CartsActivity.this);

                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CartsActivity.this);
                                rv_cart.setLayoutManager(layoutManager);
                                rv_cart.setAdapter(adapter);

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    };
                    fitfood.addListenerForSingleValueEvent(valueEventListener);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        carts.addListenerForSingleValueEvent(eventListener);

        Log.d("TOTAL", String.valueOf(sum));


    }

}
