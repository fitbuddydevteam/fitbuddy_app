package com.example.android.fitbuddy_dev;

public class fithealth_data {
    private String img;
    private String title;
    private String price;
    private  String id;

    public fithealth_data(String title, String price, String img, String id) {
        this.img = img;
        this.title = title;
        this.price = price;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
