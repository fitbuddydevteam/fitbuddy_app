package com.example.android.fitbuddy_dev;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

public class purchase_order_data_adapter extends RecyclerView.Adapter<purchase_order_data_adapter.poViewHolder>{

    private ArrayList<purchase_order_data> podatalist;
    Context context;

    public purchase_order_data_adapter(ArrayList<purchase_order_data> podatalist, Context context) {
        this.podatalist = podatalist;
        this.context = context;
    }

    @NonNull
    @Override
    public purchase_order_data_adapter.poViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_purchase_order, viewGroup, false);
        return new purchase_order_data_adapter.poViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull purchase_order_data_adapter.poViewHolder poViewHolder, int i) {
        Glide.with(context)
                .load(podatalist.get(i).getImgUrl())
                .into(poViewHolder.imgproduct);
        poViewHolder.trans_code.setText(podatalist.get(i).getTransaction_code());
        poViewHolder.nameproduct.setText(podatalist.get(i).getProduct_name());
        poViewHolder.jumlahbarang.setText(podatalist.get(i).getAmount());
        poViewHolder.hargabayar.setText(podatalist.get(i).getHarga_total());
        poViewHolder.status.setText(podatalist.get(i).getStatus());

        poViewHolder.statuspay = podatalist.get(i).getStatus();
        poViewHolder.transaction_code = podatalist.get(i).getTransaction_code();
        poViewHolder.uid = podatalist.get(i).getUid();

    }

    @Override
    public int getItemCount() {
        return podatalist.size();
    }
    public class poViewHolder extends RecyclerView.ViewHolder {
        ImageView imgproduct;
        TextView trans_code, nameproduct, jumlahbarang, hargabayar, status;
        EditText inputpass;
        Button btn_bayar, login_btn, setpass_btn;

        String transaction_code;
        String uid;
        String statuspay;


        AlertDialog.Builder dialog;
        LayoutInflater inflater;
        View dialogView;

        SharedPreferences setting;
        SharedPreferences.Editor editor;

        BottomSheetDialog bottomSheetDialog;

        public poViewHolder(@NonNull final View itemView) {
            super(itemView);

            //declaration preferences
            setting = context.getSharedPreferences("setting", Context.MODE_PRIVATE);
            editor = setting.edit();

            imgproduct = itemView.findViewById(R.id.imgproduct);
            trans_code = itemView.findViewById(R.id.trans_code);
            nameproduct = itemView.findViewById(R.id.nameproduct);
            jumlahbarang = itemView.findViewById(R.id.jumlahbarang);
            hargabayar = itemView.findViewById(R.id.hargabayar);
            status = itemView.findViewById(R.id.status);
            btn_bayar = itemView.findViewById(R.id.btn_bayar);

            //set layout inflater view
            final View inflate = LayoutInflater.from(context)
                    .inflate(R.layout.password_bottom_sheets, null);

            if(bottomSheetDialog == null){
                bottomSheetDialog = new BottomSheetDialog(context);
                bottomSheetDialog.setContentView(inflate);
            }

            btn_bayar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(setting.contains("password")){
                        bottomSheetDialog.show();

                        inputpass = inflate.findViewById(R.id.inputpass);
                        login_btn = inflate.findViewById(R.id.login_btn);
                        login_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String currentpass = setting.getString("password", "");

                                if(inputpass.getText().toString().equals(currentpass)){
                                    bottomSheetDialog.dismiss();
                                    Intent intent = new Intent(itemView.getContext(), TransferActivity.class);
                                    intent.putExtra("uid", uid);
                                    intent.putExtra("transaction_code", transaction_code);
                                    intent.putExtra("status", statuspay);
                                    itemView.getContext().startActivity(intent);
                                }else{
                                    Toast.makeText(itemView.getContext(), "Wrong Password", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else{
                        Intent intent = new Intent(itemView.getContext(), TransferActivity.class);
                        intent.putExtra("uid", uid);
                        intent.putExtra("transaction_code", transaction_code);
                        intent.putExtra("status", statuspay);
                        itemView.getContext().startActivity(intent);
                    }
                }
            });

        }
    }
}
