package com.example.android.fitbuddy_dev;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    Button logout;

    private RecyclerView rv_po;
    private purchase_order_data_adapter adapter;
    ArrayList<purchase_order_data> dataList;


    //deklarasi Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser() ;
    String uid = currentUser.getUid();
    TextView username, email;

    ProgressBar progressbar1, progressbar2;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        username = view.findViewById(R.id.username);
        email = view.findViewById(R.id.email);
        rv_po = view.findViewById(R.id.rv_po);
        progressbar1 = view.findViewById(R.id.progressbar1);
        progressbar2 = view.findViewById(R.id.progressbar2);

        readUserData();
        getPurchaseOrderData();

    }

    public void readUserData(){
        progressbar1.setVisibility(View.VISIBLE);
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query exercise = rootRef.child("user").orderByChild("uid").equalTo(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String getfullname = ds.child("firstname").getValue(String.class) + " " + ds.child("lastname").getValue(String.class);
                    String getusername = ds.child("email").getValue(String.class);

                    username.setText(getfullname);
                    email.setText(getusername);
                    progressbar1.setVisibility(View.GONE);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        exercise.addListenerForSingleValueEvent(eventListener);
    }

    public void getPurchaseOrderData(){

        dataList = new ArrayList<>();
        progressbar2.setVisibility(View.VISIBLE);
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final Query po = rootRef.child("payments").orderByChild("uid").equalTo(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String address = ds.child("address").getValue(String.class);
                    final String amount = ds.child("amount").getValue(String.class);
                    String courier = ds.child("courier").getValue(String.class);
                    String duration = ds.child("duration").getValue(String.class);
                    final String harga_total = ds.child("harga_total").getValue(String.class);
                    final String packet_id = ds.child("packet_id").getValue(String.class);
                    final String status = ds.child("status").getValue(String.class);
                    final String transaction_code = ds.child("transaction_code").getValue(String.class);
                    final String uid = ds.child("uid").getValue(String.class);

                    DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                    final Query fitfood = rootRef.child("fit-food").orderByChild("id").equalTo(packet_id);
                    ValueEventListener eventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for(DataSnapshot ds : dataSnapshot.getChildren()) {

                                String deskripsi = ds.child("deskripsi").getValue(String.class);
                                String harga = ds.child("harga").getValue(String.class);
                                String id = ds.child("id").getValue(String.class);
                                String imgUrl = ds.child("imgUrl").getValue(String.class);
                                String name = ds.child("name").getValue(String.class);
                                String stok = ds.child("stok").getValue(String.class);

                                dataList.add(new purchase_order_data(imgUrl, amount, harga_total, packet_id, transaction_code, uid, name, status));
                                adapter = new purchase_order_data_adapter(dataList, getActivity());

                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                rv_po.setLayoutManager(layoutManager);
                                rv_po.setAdapter(adapter);
                                progressbar2.setVisibility(View.GONE);


                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    };
                    fitfood.addListenerForSingleValueEvent(eventListener);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        po.addListenerForSingleValueEvent(eventListener);

    }

}
