package com.example.android.fitbuddy_dev;

public class login{
    private String email, password;

    public login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
