package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class TransferActivity extends AppCompatActivity {

    private static int RESULT_LOAD_IMG = 1;
    Uri imageUri;
    Bitmap selectedImage;
    ImageView upload_img;
    Button upload;
    ImageView preview;

    StorageReference storageReference;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        preview = findViewById(R.id.preview);
        //dekalari firebase
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("transfers");

        storageReference = FirebaseStorage.getInstance().getReference("payment");
        upload_img = findViewById(R.id.upload_img);
        upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalery();
            }
        });

        upload = findViewById(R.id.upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageUri == null){
                    Toast.makeText(TransferActivity.this, "Upload Image First", Toast.LENGTH_SHORT).show();
                }else{
                    if(getIntent().hasExtra("uid")){
                        dialog = new ProgressDialog(TransferActivity.this);
                        dialog.setMessage("please wait...");
                        dialog.show();

                        final String uid = getIntent().getStringExtra("uid");
                        final String transaction_code = getIntent().getStringExtra("transaction_code");
                        final String status = getIntent().getStringExtra("status");

                        final StorageReference ref = storageReference.child(transaction_code);
                        ref.putFile(imageUri)
                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // Get a URL to the uploaded content

                                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                String getDownloadUrl = uri.toString();
                                                transfer_data transfer_data = new transfer_data(uid, getDownloadUrl, transaction_code, status);
                                                databaseReference.child(transaction_code)
                                                        .setValue(transfer_data);
                                                Intent intent = new Intent(TransferActivity.this, ProfileActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                TransferActivity.this.startActivity(intent);
                                            }
                                        });

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        Toast.makeText(TransferActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }
        });
    }

    public void openGalery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
                preview.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(TransferActivity.this, "failed", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(TransferActivity.this, "Pilih gambar dulu",Toast.LENGTH_LONG).show();
        }
    }
}
