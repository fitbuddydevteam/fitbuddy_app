package com.example.android.fitbuddy_dev;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class fithealth extends AppCompatActivity {

    private RecyclerView rv_article;
    private fithealth_adapter adapter;
    private ArrayList<fithealth_data> articleArrayList;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fithealth);

        new HealthData(this).execute();

    }

    private class HealthData extends AsyncTask<Void, Void, Void> {


        public HealthData(fithealth activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Doing something, please wait.");
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... args) {
            readHealthData();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // do UI work here


        }
    }

    public void readHealthData(){
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference fithealth = rootRef.child("fit-health");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                articleArrayList = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    String id = ds.getKey();
                    String title = ds.child("title").getValue(String.class);
                    String date = ds.child("date").getValue(String.class);
                    String imgUrl = ds.child("imgUrl").getValue(String.class);


                    articleArrayList.add(new fithealth_data(title, date, imgUrl, id));


                    adapter = new fithealth_adapter(articleArrayList, fithealth.this);
                    rv_article = findViewById(R.id.rv_article);

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(fithealth.this);
                    rv_article.setLayoutManager(layoutManager);
                    rv_article.setAdapter(adapter);
                    dialog.dismiss();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        fithealth.addListenerForSingleValueEvent(eventListener);
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
